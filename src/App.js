import React, { Component } from 'react';
import Page, { Grid, GridColumn } from '@atlaskit/page';
import MainBody from './components/MainBody';
import Sidebar from './components/Sidebar';
import './App.css';

export const { AP } = window;

export default class App extends Component {
  state = {
    selectedMood: null,
    userName: null,
  };

  componentDidMount() {
    AP.user.getUser((user) => {
      this.setState({ userName: user.fullName });
    });
  }

  onMoodChange = (event) => {
    const { value } = event.target;
    this.setState({
      selectedMood: value,
    });
  };

  render() {
    const { selectedMood, userName } = this.state;

    return (
      <Page>
        <Grid>
          <GridColumn medium={8}>
            <MainBody
              name={userName}
              selectedMood={selectedMood}
              onMoodChange={this.onMoodChange}
            />
          </GridColumn>
          <GridColumn medium={4}>
            <Sidebar selectedMood={selectedMood} />
          </GridColumn>
        </Grid>
      </Page>
    );
  }
}
