import React from 'react';
import PropTypes from 'prop-types';
import Emojify from 'react-emojione';

MoodDisplay.propTypes = {
  selectedMood: PropTypes.string,
};

MoodDisplay.defaultProps = {
  selectedMood: null,
};

export default function MoodDisplay({ selectedMood }) {
  let emoji = null;

  switch (selectedMood) {
    case 'sad':
      emoji = ':(';
      break;
    case 'happy':
      emoji = ':)';
      break;
    case 'neutral':
      emoji = '😐';
      break;
    default:
      emoji = null;
  }

  return <Emojify style={{ height: 128, width: 128 }}>{emoji}</Emojify>;
}
