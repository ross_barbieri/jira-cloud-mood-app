import React from 'react';
import PropTypes from 'prop-types';
import MoodSelector from './MoodSelector';

MainBody.propTypes = {
  name: PropTypes.string,
  onMoodChange: PropTypes.func.isRequired,
};

MainBody.defaultProps = {
  name: 'Player 1',
};

export default function MainBody({ name, onMoodChange }) {
  return (
    <div>
      <h2>{`Hi ${name}, How do you feel today?`}</h2>
      <MoodSelector onMoodChange={onMoodChange} />
    </div>
  );
}
