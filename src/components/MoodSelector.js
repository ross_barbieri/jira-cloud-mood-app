import React from 'react';
import { RadioGroup } from '@atlaskit/radio';
import PropTypes from 'prop-types';

MoodSelector.propTypes = {
  onMoodChange: PropTypes.func.isRequired,
};

const MOOD_OPTIONS = [
  { name: 'mood', value: 'sad', label: 'Sad' },
  { name: 'mood', value: 'neutral', label: 'Neutral' },
  { name: 'mood', value: 'happy', label: 'Happy' },
];

export default function MoodSelector({ onMoodChange }) {
  return <RadioGroup options={MOOD_OPTIONS} onChange={onMoodChange} />;
}
