import React from 'react';
import PropTypes from 'prop-types';
import MoodDisplay from './MoodDisplay';

Sidebar.propTypes = {
  selectedMood: PropTypes.string.isRequired,
};

export default function Sidebar({ selectedMood }) {
  return (
    <div>
      <h2>Your Mood</h2>
      <MoodDisplay selectedMood={selectedMood} />
    </div>
  );
}
