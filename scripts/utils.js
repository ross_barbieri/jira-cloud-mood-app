const fs = require('fs');
const path = require('path');

exports.setDescriptorUrl = (baseUrl) => {
  const configPath = path.join(__dirname, '..', 'public', 'atlassian-connect.json');
  fs.readFile(configPath, (err, data) => {
    const descriptor = JSON.parse(data);
    descriptor.baseUrl = baseUrl;
    fs.writeFile(configPath, JSON.stringify(descriptor, null, 2), (error, result) => {
      if (error) {
        process.exit(1);
      }
    });
  });
};
