# Jira Cloud Mood App

This is a simple React Jira Cloud App based on [Create React App](https://github.com/facebook/create-react-app).  I've made minimal changes to allow it to run on Jira Cloud, namely:

1. Added an Atlassian Connect descriptor, `atlassian-connect.json`.
2. Added a loader to download the Atlassian JavaScript client library to index.html.  This loads the hosted version, you can also use the static version.  See [JavaScript Client docs](https://developer.atlassian.com/cloud/jira/platform/about-the-javascript-api/) for details.
3. Augmented the react start script to start ngrok on port 3000 and copy the ngrok url to the clipboard.
4. Added some very simple components to allow a user to select and display their mood.

That's basically it, hope you find this useful!

## Getting started

1. `git clone https://bitbucket.org/ross_barbieri/jira-cloud-mood-app.git`
2. `npm start`
3. Browse to your Altassian Jira Cloud instance -> Manage Apps
    https://{your_instance}.atlassian.net/plugins/servlet/upm?source=side_nav_manage_addons
4. Ensure that `Enable development mode` is checked in settings
5. Click "Upload app" and paste in ngrok url that was automatically copied when `npm start` was run.

## Modified files

The following files were added and/or modified from the vanilla CRA

`public/index.html`

```
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#000000">
    <link rel="manifest" href="%PUBLIC_URL%/manifest.json">
    <link rel="shortcut icon" href="%PUBLIC_URL%/favicon.ico">
    <!-- <script src="https://connect-cdn.atl-paas.net/all.js" data-options="sizeToParent:true;base:true;margin:true"></script> -->
    <title>Jira Mood</title>
  </head>
  <body>
    <noscript>
      You need to enable JavaScript.
    </noscript>
    <div class="ac-content">
      <div id="root"/>
    </div>
    <script id="connect-loader" data-options="sizeToParent:true;">
      (function() {
        var getUrlParam = function (param) {
            var codedParam = (new RegExp(param + '=([^&]*)')).exec(window.location.search)[1];
            return decodeURIComponent(codedParam);
        };

        var baseUrl = getUrlParam('xdm_e') + getUrlParam('cp');
        var options = document.getElementById('connect-loader').getAttribute('data-options');

        var script = document.createElement("script");
        script.src = baseUrl + '/atlassian-connect/all.js';

        if(options) {
            script.setAttribute('data-options', options);
        }

        document.getElementsByTagName("head")[0].appendChild(script);
      })();
    </script>
  </body>
</html>

```

`public/atlassian-connect.json`

```
{
  "key": "feedthree-jira-mood",
  "name": "Jira Mood",
  "description": "The Jira Mood App",
  "vendor": {
    "name": "Feedthree (An Appfire Company)",
    "url": "www.appfire.com"
  },
  "baseUrl": "https://0dc72eed.ngrok.io",
  "authentication": {
    "type": "none"
  },
  "scopes": [
    "READ",
    "WRITE"
  ],
  "modules": {
    "jiraProjectPages": [
      {
        "key": "feedthree-jira-mood-page",
        "name": {
          "value": "Jira Mood"
        },
        "url": "/index?projectKey=${project.key}&pkey={project.key}&pid={project.id}",
        "iconUrl": "/favicon.ico",
        "weight": 0
      }
    ]
  }
}

```

`scripts/start-local.sh`

```
const ngrok = require('ngrok');
const cp = require('copy-paste');
const utils = require('./utils');

console.log('starting ngrok...');

ngrok.connect(3000).then((url, err) => {
  console.log(`ngrok started on ${url}`);
  utils.setDescriptorUrl(url);
  cp.copy(`${url}/atlassian-connect.json`);
});

```

`scripts/start-local.sh`

```
const fs = require('fs');
const path = require('path');

exports.setDescriptorUrl = (baseUrl) => {
  const configPath = path.join(__dirname, '..', 'public', 'atlassian-connect.json');
  fs.readFile(configPath, (err, data) => {
    const descriptor = JSON.parse(data);
    descriptor.baseUrl = baseUrl;
    fs.writeFile(configPath, JSON.stringify(descriptor, null, 2), (err, result) => {
      if (err) {
        console.error(err);
        process.exit(1);
      }
    });
  });
};

```

`package.json`

`"start": "node scripts/start-local.js & react-scripts start"`

# Original Create React App notes

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
